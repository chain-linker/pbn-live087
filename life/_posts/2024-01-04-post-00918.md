---
layout: post
title: "평택석정공원 화성파크드림 분양가 평면도 시세 단지배치도 모델하우스"
toc: true
---

 평택석정공원 화성파크드림 분양공고가 2022년 10월 게시되었습니다. 평택 석정공원 화성파크드림의 분양가와 평면도, 아파트 시세와 불과시 배치도 모델하우스 등에 대해 알아보겠습니다. 평택 장당동에 위치한 평택석정공원 화성파크드림 아파트는 평택 석정공원 민간공원 특례사업을 통해 분양되는 아파트로 화성산업에서 시공을 맡았습니다. 중대형 평형인 32평~43평 1,296세대 대단지 아파트로 전 세대가 금차 일반분양을 통해 분양됩니다.
 평택석정공원 화성파크드림의 분양가는 4억 600만 원~5억 8,700만 원으로 32평 분양가는 4억 600만 원~4억 5,800만 원, 33평 분양가는 4억 1,500만 원~4억 6,800만 원, 43평 분양가는 5억 2,600만 원~5억 8,700만 원입니다.

 

 -아파트 명칭: 평택석정공원 화성파크드림
 -주소: 경기도 평택시 장당동 세기 12-5번지
 -대지면적: 50,751㎡(약 15,352평)
 -용도지역: 제3종 일반주거지역
 -연면적: 216,225.8324㎡(약 65,408평)
 -용적률: 287.65%
 -건폐율: 17.43%
-공급규모: 지하 4층 ~ 일생 최상 29층, 아파트 11개동 및 근린생활시설(상가)

 -세대수: 1,296세대(일반공급 635세대)
-아파트 평형: 32평~43평(전용 80㎡~110㎡)
 -주차대수: 1,696대

 -세대당 주차대수: 1.30대
 -난방 방식: 개별난방(도시가스)
 -입주시기: 2025년 7월 입주 예정
 -시행 위탁사: 평택석정파크드림 주식회사
-시행 수탁사: 주식회사 하나자산신탁
-시공사: 화성산업 주식회사
 

 평택시의 민간공원 [평택브레인시티중흥](https://squirrel-grape.com/life/post-00080.html) 특례사업 아파트답게 석정공원을 품은 아파트로 조성이 될 예정입니다.

## 위치 및 입지분석

### 교통여건
 1호선 서정리역
경기대로, 청원로, 평택~제천 고속도로

### 교육환경
 장당초, 장당중, 효명중, 효명고
카이스트 평택캠퍼스(2024년 개교 예정), 국립 한국복지대학교

### 편의/문화/의료시설
 롯데시네마
장당도서관
송탄보건소, 평택 아주대학교 병원(2027년 개원 예정)

### 일자리
 삼성전자 평택캠퍼스, 장당 일반산업단지, 송탄 일반산업단지
평택시청 송탄출장소

### 자연환경
 석정 근린공원, 서정공원
 
## 일반분양 세대수

 아파트는 총 1,296세대로 일반공급은 635세대, 특별공급은 661세대(기관추천 특별공급 124세대, 다자녀가구 특별공급 129세대, 신혼부부 특별공급 248세대, 노부모 부양 특별공급 36세대, 생애최초 특별공급 124세대)가 분양됩니다.
 

## 분양가격

 80A 분양가: 4억 1,000만 원~4억 5,800만 원
80B 분양가: 4억 600만 원~4억 5,300만 원
84A 분양가: 4억 1,900만 원~4억 6,800만 원
84B 분양가: 4억 1,500만 원~4억 6,400만 원
84C 분양가: 4억 1,500만 원~4억 6,400만 원
84D 분양가: 4억 1,900만 원~4억 6,800만 원
110 분양가: 5억 2,600만 원~5억 8,700만 원
 

 평택석정공원 화성파크드림 아파트의 중도금은 이자후불제이며 대출은 유이자입니다. 분양가의 60% 이내에서 취급이 가능합니다.

## 발코니 확장비

 발코니 확장비용은 최저 1,800만 원~최고 2,150만 원입니다.
 

## 평택 장당동 아파트 시세

 평택 장당 우미이노스빌 1차 34평 3억 4,000만 원(2005년 입주)
평택 장당 우미이노스빌 2차 34평 3억 4,300만 원(2005년 입주)
평택 장당 우미이노스빌 3차 32평 3억 4,200만 원(2006년 입주)
장당 한국아델리움 35평 4억 원(2005년 입주)

 밑면 글은 이사이 평택에 분양한 아파트 분양공고문입니다.
 

 

## 가을걷이 선택 품목(유상 옵션)

### 1) 시스템에어컨 옵션

### 2) 인테리어 유상 옵션

 거실 아트월 세라믹 타일, 주방 꼴 UP 옵션, 주방 상판/벽체 세라믹 타일, 거실/주방 밑 포세린 타일, 실총 원목마루 옵션이 선택 가능합니다.

### 3) 주방가전 유상 옵션

 냉장고, 변온고, 김치냉장고, 키 큰 장, 와인랙, 식기세척기, 쿡탑을 옵션으로 선택이 가능합니다.

### 4) 수납 옵션

### 5) 우수리 유상 옵션

 현관 중문, 현관 에어샤워 시스템, 욕실 힘펠 환풍기, 조명 특화 옵션이 있습니다.

## 겨우 배치도

 아파트 단지는 양지 공원화 아파트로 리프레쉬 가든, 키즈랜드, 드림랜드, 에버그린파크, 건강마당, 물빛 정원, EQ동산, 이팝나무 가로수길 등으로 꾸며집니다. 주출입구(정문) 쪽에는 근린생활시설(상가)이 조성되며 108동 옆으로 석정 근린공원으로 곧 이어지는 산책로가 조성됩니다.

## 아파트 커뮤니티 시설

 한개 내 커뮤니티로는 지하 1층에 주민카페와 맘&키즈카페, 지하 2층에는 키즈스테이션, 프리미엄 독서실, 경로당, 지하 3층에는 피트니스(헬스장), 필라테스룸, 각시 골프연습장, 스크린골프장이 설치됩니다.

## 동호수 배치도

 101동부터 111동까지 11개동이 있으며 아파트의 최상 층수는 21층~29층입니다.

## 평면도

 80A는 32평으로 방 4개와 화장실 2개가 있는 4베이 구조입니다.
 

 아래의 사진은 모델하우스에 전시된 80A타입 인테리어 사진입니다.

 80B타입은 141세대가 있으며 방 3개와 알파룸 1개, 화장실 2개가 있는 타워형 구조의 세대입니다.

 84A타입은 150세대가 분양되며 방 4개와 화장실 2개가 있는 판상형 4베이 구조입니다.

 

 

 84C타입은 190세대가 있습니다.

 84D타입은 371세대가 있으며 방 3개와 화장실 2개가 있는 4베이 판상형 구조입니다.

 전용 110타입은 43평으로 4개의 방과 2개의 욕실이 있는 4BAY 구조입니다.

## 청약 1순위 자격/청약 예치금액/특별공급 청약자격 안내

 1순위 자격, 청약 예치금액, 특별공급 공통사항 안내, 부양가족 산정기준과 무주택 근기 산정방법 등에 관한 설명입니다.

## 청약 체크리스트

## 청약 신청 사이트

 https://www.applyhome.co.kr/
 

## 청약일정(분양일정)

 최초 입주자 모집공고일: 2022년 10월 7일

 모델하우스 오픈일자: 2022년 10월 14일(금)

 특별공급 청약일: 2022년 10월 17일  

 일반공급 1순위 청약접수일: 2022년 10월 18일  

 일반공급 2순위 청약접수일: 2022년 10월 19일

 청약 당첨자 발표일: 2022년 10월 27일  

 청약 당첨자 글 접수: 2022년 10월 30일~11월 2일
 청약 당첨자 정당계약: 2022년 11월 7일~11일
 

 모하는 내일인 10월 14일 오픈 예정입니다.

## 모델하우스 및 분양현장 위치

 모델하우스 주소: 경기도 평택시 소사동 산 2-8번지
 

 결과 분양 관련 내용은 평택석정공원 화성파크드림 입주자 모집공고문을 확인해주시기 바랍니다.

## 평택석정공원 화성파크드림 분양 홈페이지 주소
 http://seokjeong-parkdream.com/
 

 이상으로 ooo아파트의 분양가 평면도 물정 단지배치도 모델하우스 등에 관한 포스팅을 마치겠습니다. 감사합니다.
